package main

import (
	"context"
	"fmt"
	"log"
	"net/http"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func main() {
	// Koneksi ke database MongoDB
	client, err := mongo.Connect(context.Background(), options.Client().ApplyURI("mongodb://mongo-1.mongo.default.svc.cluster.local:27017"))
	if err != nil {
		log.Fatal(err)
	}

	// Pilih database "mydatabase"
	database := client.Database("mydatabase")

	// Pilih collection "people"
	//test branch aku adalah penyuka anime
	collection := database.Collection("people")

	// Buat cursor untuk membaca semua dokumen dalam collection
	cursor, err := collection.Find(context.Background(), bson.M{})
	if err != nil {
		log.Fatal(err)
	}

	// Deklarasikan variabel untuk menampung data JSON
	var results []bson.M

	// Iterate melalui cursor dan decode setiap dokumen ke JSON
	err = cursor.All(context.Background(), &results)
	if err != nil {
		log.Fatal(err)
	}

	// Buat handler HTTP untuk menampilkan data JSON
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		for _, result := range results {
			fmt.Fprintf(w, "%v\n", result)
		}
	})

	// Jalankan server HTTP di localhost:80
	log.Fatal(http.ListenAndServe(":8080", nil))
}
