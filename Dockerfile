# Gunakan image Golang yang resmi sebagai base image
FROM golang:1.18

# Buat direktori kerja untuk aplikasi
WORKDIR /app

# Copy file Go ke dalam direktori kerja
COPY ./src/ .

# Download dependensi Go
RUN go mod download

# Build aplikasi Go
RUN go build -o main

# Tentukan port yang akan dibuka
EXPOSE 8080

# Jalankan aplikasi saat container dimulai
CMD ["./main"]
